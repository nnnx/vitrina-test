<?php

namespace app\commands;


use Yii;
use app\models\User;
use yii\console\Controller;

class RbacController extends Controller {

    const
        DEFAULT_EMAIL = 'top60@yandex.ru',
        DEFAULT_PASSWORD = '123123';

    public function actionInit() {
        $authManager = Yii::$app->authManager;
        $authManager->removeAll();

        foreach (User::$roleLabels as $roleKey => $roleLabel) {
            $role = $authManager->createRole($roleKey);
            $role->description = $roleLabel;
            $authManager->add($role);
        }

        $user = User::find()->where(['email' => self::DEFAULT_EMAIL])->one();
        if (!$user) {
            $user = new User();
            $user->setAttributes([
                'email' => self::DEFAULT_EMAIL
            ]);
            $user->setPassword(self::DEFAULT_PASSWORD);
            $user->generateAuthKey();
            if (!$user->save()) {
                print_r($user->errors);
            }
        }
        $authManager->assign($authManager->getRole(User::ROLE_ADMIN), $user->id);
        Yii::$app->cache->delete('rbac');

        echo "Success!\n";
    }
}
