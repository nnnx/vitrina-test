<?php

namespace app\models\form;

use app\models\User;
use Yii;
use yii\base\Model;

class LoginForm extends Model {

    public $login, $password;

    protected $_user;

    public function rules() {
        return [
            [['login', 'password'], 'required', 'message' => 'Обязательно']
        ];
    }

    public function attributeLabels() {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    /**
     * Finds user by login
     *
     * @return User|null
     */
    protected function getUser() {
        if ($this->_user === null) {
            $this->_user = User::findByLogin($this->login);
        }

        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login() {
        $user = $this->getUser();
        if ($user && $user->validatePassword($this->password)) {
            return Yii::$app->user->login($this->getUser(), 3600 * 24 * 30);
        }

        return false;
    }
}