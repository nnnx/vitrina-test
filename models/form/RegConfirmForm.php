<?php

namespace app\models\form;

use app\models\User;
use Yii;
use yii\base\Model;

class RegConfirmForm extends Model {

    public $password, $password_repeat;

    /** @var User */
    protected $_user;

    public function rules() {
        return [
            [['password', 'password_repeat'], 'required', 'message' => 'Обязательно'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают']
        ];
    }

    public function attributeLabels() {
        return [
            'password' => 'Пароль',
            'password_repeat' => 'Повторите пароль',
        ];
    }

    /**
     * @param $str
     * @return User|null
     */
    public function setToken($str) {
        $this->_user = User::findByPasswordResetToken($str);

        return $this->_user;
    }

    /**
     * обработка формы
     * @throws \Exception
     */
    public function process() {
        if (!$this->_user) {
            throw new \Exception('Не указан пользователь');
        }
        $this->_user->updated_at = time();
        $this->_user->setPassword($this->password);
        $this->_user->generateAuthKey();
        $this->_user->removePasswordResetToken();
        $this->_user->save();
    }
}