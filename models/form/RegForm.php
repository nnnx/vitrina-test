<?php

namespace app\models\form;

use app\models\service\Email;
use app\models\service\Sms;
use app\models\User;
use Yii;
use yii\base\Model;

class RegForm extends Model {

    const
        TYPE_EMAIL = 'email',
        TYPE_PHONE = 'phone';

    public $login;

    public function rules() {
        return [
            [['login'], 'required', 'message' => 'Обязательно'],
            ['login', 'validateLogin']
        ];
    }

    public function validateLogin($attr, $field) {
        $type = $this->_extractType();
        if ($type === false) {
            $this->addError($attr, 'Не является корректным адресом эл. почты или номером телефона');
        } else {
            $hasDuplicate = User::find()->where([$type => $this->login])->count();
            if ($hasDuplicate) {
                $this->addError($attr, 'Этот логин уже используется');
            }
        }
    }

    public function attributeLabels() {
        return [
            'login' => 'Логин (email или номер телефона)'
        ];
    }

    /**
     * получить тип логина
     * @return bool|string
     */
    protected function _extractType() {
        if (filter_var($this->login, FILTER_VALIDATE_EMAIL)) {
            return self::TYPE_EMAIL;
        }

        if (preg_match('/^\d{11}$/', $this->login)) {
            return self::TYPE_PHONE;
        }

        return false;
    }

    /**
     * @param int $length
     * @return bool|string
     */
    protected function _genPassword($length = 6) {
        $symbols = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle($symbols), 0, $length);
    }

    /**
     * обработка формы
     * @return bool|string
     * @throws \Exception
     */
    public function process() {
        $authManager = Yii::$app->authManager;
        $type = $this->_extractType();
        $password = $this->_genPassword();
        $user = new User;
        $user->setAttributes([
            'created_at' => time(),
            'updated_at' => time()
        ]);
        $user->setAttribute($type, $this->login);
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->save();
        $authManager->assign($authManager->getRole(User::ROLE_USER), $user->id);
        Yii::$app->cache->delete('rbac');

        if ($type == self::TYPE_PHONE) {
            Sms::send($this->login, 'Пароль: ' . $password);
        }

        if ($type == self::TYPE_EMAIL) {
            $user->generatePasswordResetToken();
            $user->save();
            $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']), array('off', 'no'))) ? 'https' : 'http';
            $url .= '://' . $_SERVER['HTTP_HOST'] . '/site/reg-confirm?token=' . $user->password_reset_token;
            Email::send($this->login, 'Регистрация', 'Чтобы продолжить регистрацию перейдите по ссылке ' . $url);
        }

        return $type;
    }
}