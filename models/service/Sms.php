<?php

namespace app\models\service;


class Sms {

    /**
     * @param $number номер
     * @param $text сообщение
     * @throws \Exception
     */
    public static function send($number, $text) {
        $ch = curl_init('https://sms.ru/sms/send');
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POSTFIELDS => [
                'api_id' => \Yii::$app->params['smsApiId'],
                'to' => $number,
                'msg' => $text,
                'json' => 1
            ]
        ]);
        $body = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($body);
        if ($json && $json->status_code == 100) {
            foreach ($json->sms as $phone => $data) {
                if ($data->status_code != 100) {
                    throw new \Exception($data->status_text);
                }
            }
        } else {
            throw new \Exception('Запрос не выполнился. Не удалось установить связь с сервером.');
        }
    }
}