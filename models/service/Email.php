<?php

namespace app\models\service;

use PHPMailer\PHPMailer\PHPMailer;

class Email {

    /**
     * @param $address куда
     * @param $subj тема
     * @param $body текст
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public static function send($address, $subj, $body) {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->Port = 465;
        $mail->Host = \Yii::$app->params['mail']['host'];
        $mail->SMTPAuth = true;
        //$mail->SMTPDebug = 1;
        $mail->Username = \Yii::$app->params['mail']['username'];
        $mail->Password = \Yii::$app->params['mail']['password'];
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPAutoTLS = false;
        $mail->From = \Yii::$app->params['mail']['username'];
        $mail->FromName = 'noreply';
        $mail->addAddress($address);
        $mail->isHTML(true);
        $mail->Subject = $subj;
        $mail->Body = $body;
        if ($mail->send() === false) {
            throw new \Exception('Произошла ошибка отправки email');
        }
    }
}