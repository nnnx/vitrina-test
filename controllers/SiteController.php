<?php

namespace app\controllers;

use app\models\form\RegConfirmForm;
use app\models\form\RegForm;
use app\models\form\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap\ActiveForm;

class SiteController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'reg', 'reg-confirm', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    return $this->redirect(['login']);
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index', [
            'user' => Yii::$app->user->identity
        ]);
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $errors = ActiveForm::validate($model);
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $errors;
            }

            if ($model->login()) {
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash(uniqid(), ['error', 'Неверный логин или пароль']);
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionReg() {
        $model = new RegForm();
        if ($model->load(Yii::$app->request->post())) {
            $errors = ActiveForm::validate($model);
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $errors;
            }

            try {
                return $this->render('reg-success', ['type' => $model->process()]);
            } catch (\Exception $e) {
                return $this->render('reg-fail');
            }
        }

        return $this->render('reg', [
            'model' => $model
        ]);
    }

    public function actionRegConfirm() {
        $model = new RegConfirmForm();
        if ($model->setToken(Yii::$app->request->get('token'))) {
            if ($model->load(Yii::$app->request->post())) {
                $errors = ActiveForm::validate($model);
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $errors;
                }
                $model->process();
                Yii::$app->session->setFlash(uniqid(), ['success', 'Пароль успешно обновлен']);

                return $this->redirect(['login']);
            }

            return $this->render('reg-confirm', ['model' => $model]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
