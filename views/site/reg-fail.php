<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\form\RegForm;

?>

<div class="auth-form-ctr animated fadeInDown">
    <div class="auth-form-header">
        <a href="<?= Url::to(['login']); ?>" class="item">Вход</a>
        <a href="<?= Url::to(['reg']); ?>" class="item active">Регистрация</a>
    </div>

    <div class="auth-form-content">
        <div class="alert alert-danger">Произошла ошибка регистрации. Повторите попытку</div>
    </div>
</div>