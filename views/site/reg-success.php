<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\form\RegForm;

?>

<div class="auth-form-ctr animated fadeInDown">
    <div class="auth-form-header">
        <a href="<?= Url::to(['login']); ?>" class="item">Вход</a>
        <a href="<?= Url::to(['reg']); ?>" class="item active">Регистрация</a>
    </div>

    <div class="auth-form-content">

        <?php if ($type == RegForm::TYPE_PHONE) { ?>
            <div class="alert alert-success">Регистрация прошла успешно.<br/>Пароль для входа был отправлен на ваш номер телефона</div>
            <a href="<?= Url::to(['login']); ?>" class="btn btn-primary">Перейти к логину</a>
        <?php } else { ?>
            <div class="alert alert-success">Проверьте свою почту.<br/>Вам отправлено письмо для подтверждения почты и установки пароля</div>
        <?php } ?>
    </div>
</div>