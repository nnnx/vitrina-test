<?php

/** @var $user \app\models\User */

use yii\helpers\Url;

?>

<div class="auth-form-ctr animated fadeInDown">
    <div class="auth-form-content">
        Вы успешно авторизованы под логином:<br/>
        <code><?= $user->login; ?></code>
        <div class="hr-line-dashed"></div>
        <a href="<?= Url::to(['logout']); ?>" class="btn btn-primary"><i class="fa fa-lock"></i> Выйти</a>
    </div>
</div>