<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>

<div class="auth-form-ctr animated fadeInDown">
    <div class="auth-form-header">
        <a href="<?= Url::to(['login']); ?>" class="item">Вход</a>
        <a href="<?= Url::to(['reg']); ?>" class="item active">Регистрация</a>
    </div>

    <div class="auth-form-content">
        <?php
        $formId = uniqid();
        $form = ActiveForm::begin([
            'method' => 'post',
            'options' => [
                'class' => '',
                'id' => $formId
            ],
            'validateOnType' => true,
            'enableAjaxValidation' => true,
        ]);
        ?>

        <?= $form->field($model, 'login'); ?>

        <small><i class="fa fa-info-circle"></i> формат номера телефона 7XXXXXXXXXX</small>

        <div class="hr-line-dashed"></div>

        <button type="submit" class="btn btn-primary">Зарегистрироваться</button>

        <?php ActiveForm::end(); ?>
    </div>
</div>