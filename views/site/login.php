<?php

/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>

<div class="auth-form-ctr animated fadeInDown">
    <div class="auth-form-header">
        <a href="<?= Url::to(['login']); ?>" class="item active">Вход</a>
        <a href="<?= Url::to(['reg']); ?>" class="item">Регистрация</a>
    </div>

    <div class="auth-form-content">
        <?php
        $formId = uniqid();
        $form = ActiveForm::begin([
            'method' => 'post',
            'options' => [
                'class' => '',
                'id' => $formId
            ],
            'validateOnType' => true,
            'enableAjaxValidation' => true,
        ]);
        ?>

        <?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <div class="hr-line-dashed"></div>
        <button type="submit" class="btn btn-primary"><i class="fa fa-lock"></i> Войти</button>

        <?php ActiveForm::end(); ?>
    </div>
</div>