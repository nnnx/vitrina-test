<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>

<div class="auth-form-ctr animated fadeInDown">
    <div class="auth-form-header">
        <span class="item active">Подтверждение почты</span>
    </div>

    <div class="auth-form-content">
        <?php
        $formId = uniqid();
        $form = ActiveForm::begin([
            'method' => 'post',
            'options' => [
                'class' => '',
                'id' => $formId
            ],
            'validateOnType' => true,
            'enableAjaxValidation' => true,
        ]);
        ?>

        <?= $form->field($model, 'password')->passwordInput(); ?>
        <?= $form->field($model, 'password_repeat')->passwordInput(); ?>

        <div class="hr-line-dashed"></div>

        <button type="submit" class="btn btn-primary">Сохранить пароль</button>

        <?php ActiveForm::end(); ?>
    </div>
</div>