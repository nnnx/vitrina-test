<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle {

    const VERSION = '1.0';

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.min.css',
        'css/plugins/toastr/toastr.min.css',
        'css/animate.css',
        'css/inspinia.css',
        'css/custom.css',
    ];

    public $js = [
        'js/bootstrap.min.js',
        'js/plugins/toastr/toastr.min.js',
        'js/moment-with-locales.js',
        'js/inspinia.js',
        'js/script.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\AssetBundle',
        'yii\bootstrap\BootstrapAsset',
        'yii\validators\ValidationAsset',
        'yii\widgets\MaskedInputAsset',
        'yii\widgets\ActiveFormAsset'
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public function init() {
        foreach ($this->js as $key => $value) {
            $this->js[$key] = $value . '?v=' . self::VERSION;
        }

        foreach ($this->css as $key => $value) {
            $this->css[$key] = $value . '?v=' . self::VERSION;
        }

        parent::init();
    }
}
